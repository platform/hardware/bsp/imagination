#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := hardware/bsp/imagination/soc/fs1130

# MIPS32 device.
TARGET_ARCH := mips
TARGET_ARCH_VARIANT := mips32r2-fp
TARGET_CPU_VARIANT :=
TARGET_CPU_ABI := mips
TARGET_CPU_ABI2 :=
TARGET_KERNEL_ARCH := $(TARGET_ARCH)

TARGET_NO_BOOTLOADER := false
TARGET_NO_KERNEL := false

BOARD_KERNEL_CMDLINE := console=ttyS1,115200n8 earlycon=uart8250,mmio32,0x18101500,115200 androidboot.console=ttyS1 androidboot.hardware=fs1130 security=selinux androidboot.selinux=enforcing

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 131072

PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/init.fs1130.rc:root/init.fs1130.rc \
  system/core/rootdir/init.usb.rc:root/init.usb.rc \
  system/core/rootdir/init.usb.configfs.rc:root/init.usb.configfs.rc \
  $(LOCAL_PATH)/init.usb.fs1130.rc:root/init.usb.fs1130.rc \
  system/core/rootdir/ueventd.rc:root/ueventd.rc \
  $(LOCAL_PATH)/ueventd.fs1130.rc:root/ueventd.fs1130.rc \

BOARD_SEPOLICY_DIRS += \
  $(LOCAL_PATH)/sepolicy \

TARGET_KERNEL_SRC := hardware/bsp/kernel/imagination/v4.1
TARGET_KERNEL_DEFCONFIG := pistachio_defconfig
TARGET_KERNEL_CONFIGS := $(TARGET_KERNEL_CONFIGS) $(realpath $(LOCAL_PATH)/soc.kconf)

# Keystore HAL
DEVICE_PACKAGES += \
  keystore.default

# Boot control HAL
DEVICE_PACKAGES += \
  bootctrl.fs1130

# Device partition table
DEVICE_PACKAGES += \
  gpt.bin

